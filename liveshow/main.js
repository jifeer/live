import Vue from 'vue'
import App from './App'
import store from './store'
import $mUtils from './common/utils.js' // 常用函数

Vue.config.productionTip = false

Vue.prototype.$store = store
Vue.prototype.$mUtils= $mUtils
App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
